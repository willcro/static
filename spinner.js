let historyTime = [];
let historyAngle = [];
let HISTORY_SIZE = 10;
let speed = 0;

let lastTimestamp = new Date().getTime();
let lastAngle = 0;

window.addEventListener("deviceorientation", (e) => {
  // document.body.style.transform = `rotate(${e.alpha}deg)`
  historyTime.push(e.timeStamp);
  historyTime = historyTime.slice(-HISTORY_SIZE);

  var normalizedAlpha = (e.alpha + 360) % 360;
  var calculatedAlpha = normalizedAlpha;
  if (historyAngle.length > 0) {
    var upOne = normalizedAlpha + 360;
    var downOne = normalizedAlpha - 360;
    var previousAlpha = historyAngle[historyAngle.length-1];
    if (Math.abs(upOne - previousAlpha) < Math.min(downOne - previousAlpha)) {
      calculatedAlpha = upOne;
    } else {
      calculatedAlpha = downOne;
    }
  }

  historyAngle.push(e.alpha);
  historyAngle = historyAngle.slice(-HISTORY_SIZE);

  let calcSpeed = regress(historyTime, historyAngle).slope;
  speed = isNaN(calcSpeed) ? 0 : calcSpeed;
});

setInterval(() => console.log(`Speed: ${speed}`), 1000);

function renderFrame() {
  var newTimestamp = new Date().getTime();
  var newAngle = lastAngle + (speed * (newTimestamp - lastTimestamp));
  lastTimestamp = newTimestamp;
  lastAngle = newAngle;
  document.body.style.transform = `rotate(${newAngle}deg)`;
  console.log(newAngle);
  requestAnimationFrame(renderFrame);
}

renderFrame();

const regress = (x, y) => {
  const n = y.length;
  let sx = 0;
  let sy = 0;
  let sxy = 0;
  let sxx = 0;
  let syy = 0;
  for (let i = 0; i < n; i++) {
      sx += x[i];
      sy += y[i];
      sxy += x[i] * y[i];
      sxx += x[i] * x[i];
      syy += y[i] * y[i];
  }
  const mx = sx / n;
  const my = sy / n;
  const yy = n * syy - sy * sy;
  const xx = n * sxx - sx * sx;
  const xy = n * sxy - sx * sy;
  const slope = xy / xx;
  const intercept = my - slope * mx;
  const r = xy / Math.sqrt(xx * yy);
  const r2 = Math.pow(r,2);
  let sst = 0;
  for (let i = 0; i < n; i++) {
     sst += Math.pow((y[i] - my), 2);
  }
  const sse = sst - r2 * sst;
  const see = Math.sqrt(sse / (n - 2));
  const ssr = sst - sse;
  return {slope, intercept, r, r2, sse, ssr, sst, sy, sx, see};
}
